package com.epam.calculator;

import java.util.EmptyStackException;
import java.util.Stack;

public class Calculator {

    private String input;

    public Calculator(String postfixExpression) {
        this.input = postfixExpression.trim();
    }

    private boolean isOperator(String operator) {
        return operator.equals("+") || operator.equals("-") || operator.equals("*") || operator.equals("/");
    }

    public int calculatesExpression() throws NumberFormatException, EmptyStackException, ArithmeticException {
        String[] subExpr = input.split(" ");
        Stack<Integer> stackIntegers = new Stack<>();
        int numberFirst, numberSecond, interResult;
        for (String element : subExpr) {
            if (!isOperator(element.trim())) {
                stackIntegers.push(Integer.parseInt((element.trim())));
            } else {
                numberSecond = stackIntegers.pop();
                numberFirst = stackIntegers.pop();
                switch (element) {
                    case "+":
                        interResult = numberFirst + numberSecond;
                        break;
                    case "-":
                        interResult = numberFirst - numberSecond;
                        break;
                    case "*":
                        interResult = numberFirst * numberSecond;
                        break;
                    case "/":
                        interResult = numberFirst / numberSecond;
                        break;
                    default:
                        interResult = 0;
                }
                stackIntegers.push(interResult);
            }
        }
        interResult = stackIntegers.pop();
        return interResult;
    }
}
