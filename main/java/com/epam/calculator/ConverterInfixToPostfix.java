package com.epam.calculator;

import java.util.Stack;

public class ConverterInfixToPostfix {

    private Stack<String> stackOperators;
    private String input;
    private String output;

    public ConverterInfixToPostfix(String expression) {
        this.input = expression;
        this.stackOperators = new Stack<>();
        this.output = "";
    }

    String transPostfix() {
        String[] subExpr = input.split(" ");
        for (String element : subExpr) {
            switch (element.trim()) {
                case "+":
                case "-":
                    getOperator(element, 1);
                    break;
                case "*":
                case "/":
                    getOperator(element, 2);
                    break;
                case "(":
                    stackOperators.push(element);
                    break;
                case ")":
                    getParenthesis();
                    break;
                default:
                    output = output.concat(" ").concat(String.valueOf(element));
                    break;
            }
        }
        while (!stackOperators.empty()) {
            output = output.concat(" ").concat(String.valueOf(stackOperators.pop()));
        }
        return output;
    }

    private void getOperator(String operator, int priorityFirst) {
        while (!stackOperators.empty()) {
            String topOperator = stackOperators.pop();
            if (topOperator.equals("(")) {
                stackOperators.push(topOperator);
                break;
            } else {
                int prioritySecond;
                if (topOperator.equals("+") || topOperator.equals("-")) {
                    prioritySecond = 1;
                } else {
                    prioritySecond = 2;
                }
                if (prioritySecond < priorityFirst) {
                    stackOperators.push(topOperator);
                    break;
                } else {
                    output = output.concat(" ").concat(String.valueOf(topOperator));
                }
            }
        }
        stackOperators.push(operator);
    }

    private void getParenthesis() {
        while (!stackOperators.empty()) {
            String operator = stackOperators.pop();
            if (operator.equals("(")) {
                break;
            } else {
                output = output.concat(" ").concat(String.valueOf(operator));
            }
        }
    }
}
