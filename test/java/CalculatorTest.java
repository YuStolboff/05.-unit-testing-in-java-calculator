import com.epam.calculator.Calculator;
import com.epam.calculator.ConverterInfixToPostfix;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.EmptyStackException;

import static org.junit.Assert.assertEquals;

public class CalculatorTest {

    @Test
    public void testCalculatesAdditionPositiveScenarios() {
        assertEquals("Addition test failed", 32, new Calculator("30 2 +").calculatesExpression());
    }

    @Test
    public void testCalculatesSubtractionPositiveScenarios() {
        assertEquals("Subtraction test failed", 32, new Calculator("40 8 -").calculatesExpression());
    }

    @Test
    public void testCalculatesMultiplicationPositiveScenarios() {
        assertEquals("Multiplication test failed", 32, new Calculator("8 4 *").calculatesExpression());
    }

    @Test
    public void testCalculatesDivisionPositiveScenarios() {
        assertEquals("Division test failed", 32, new Calculator("320 10 /").calculatesExpression());
    }

    @Test(expected = java.lang.ArithmeticException.class)
    public void testCalculatesDivisionNegativeScenarios() {
        new Calculator("32 0 /").calculatesExpression();
    }

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testCalculatesEmptyStringNegativeScenarios() {
        expectedException.expect(NumberFormatException.class);
        new Calculator("").calculatesExpression();
    }

    @Test
    public void testCalculatesFewOperatorsInRowNegativeScenarios() {
        expectedException.expect(EmptyStackException.class);
        new Calculator("320 10 * *").calculatesExpression();
    }

    @Test
    public void testCalculatesNonArithmeticSymbolsNegativeScenarios() {
        expectedException.expect(NumberFormatException.class);
        new Calculator("320 10 &").calculatesExpression();
    }
}